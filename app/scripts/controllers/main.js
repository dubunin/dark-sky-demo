'use strict';

/**
 * @ngdoc function
 * @name darkSkyDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the darkSkyDemoApp. Loads location data based on IP then calls Dark Sky 
 * to get forecast data.
 */
angular.module('darkSkyDemoApp')
  .controller('MainCtrl', [
    'darkSkyProvider',
    'geoLocalProvider',
    '$scope',
    function (darkSkyProvider, geoLocalProvider, $scope) {
      $scope.forecasts = [];

      var loadLocationData = function () {
        return geoLocalProvider.getGeoLocal()
          .then(function (response) {
            if (response && response.loc) {
              var position = {
                'latitude': response.loc.split(',')[0],
                'longitude': response.loc.split(',')[1],
                'city': response.city,
                'region': response.region
              };
              return position;
            }
            else {
              scope.positionError = {
                message: 'An error occured getting the current position'
              };
              return position;
            }
          });
      },
        loadForecast = function (position) {
          return darkSkyProvider.getForecast(position.latitude, position.longitude)
            .then(function (forecastData) {
              $scope.forecastData = forecastData;
              $scope.forecasts = forecastData.daily.data.slice(0, 5);
              $scope.currentLocation = position.city + ", " + position.region;
              return forecastData;
            });
        };

      // Chain promises for loading location data then forecast data
      loadLocationData().then(loadForecast);

      // add these to scope for testing testing
      $scope.loadLocationData = loadLocationData;
      $scope.loadForecast = loadForecast;
    }]);
