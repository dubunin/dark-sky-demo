'use strict';

describe('Controller: MainCtrl', function () {

  var MainCtrl, scope, $rootScope, $httpBackend, darkSkyProvider, geoLocalProvider;

  // load the controller's module
  beforeEach(module('darkSkyDemoApp'));

  beforeEach(inject(function($controller, _$httpBackend_, _$rootScope_, 
                              _darkSkyProvider_, _geoLocalProvider_) {
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
    darkSkyProvider = _darkSkyProvider_;
    geoLocalProvider = _geoLocalProvider_;

    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should create a valid MainCtrl', function() {
    expect(MainCtrl).not.toBeUndefined();
  });

  it('should get default forecast data', function() {
    expect(scope.forecasts.length).toBe(0);
  });

  it('should get valid geo local data', function() {
    scope.loadLocationData().then(function(position){
      expect(position).not.toBeUndefined();
    });
  });

  it('should get valid forecast data', function() {
    var mockPosition = {
      'latitude': '47.7205',
      'longitude': '-122.2067',
      'city': 'Bothell',
      'region': "Washington"
    };
    scope.loadForecast(mockPosition).then(function(forecastData){
      expect(forecastData).not.toBeUndefined();
      expect(scope.forecasts).length.toEqual(5);
      expect(scope.currentLocation).toEqual('Bothel, Washington');
    });
  });

  it('should set positionError for bad position data', function() {
    var mockPosition = {
      'latitude': '-1',
      'longitude': '0'
    };
    scope.loadForecast(mockPosition).then(function(forecastData){
      expect(forecastData).toBeUndefined();
      expect(scope.forecasts).toBeUndefined();
      expect(scope.positionError).toBeDefined();
      expect(scope.positionError.message).toEqual('An error occured getting the current position');
    });
  });

});
