'use strict';

/**
 * @ngdoc overview
 * @name darkSkyDemoApp
 * @description
 * # darkSkyDemoApp
 *
 * Main module of the application.
 */
angular
  .module('darkSkyDemoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $locationProvider, $sceDelegateProvider){
    $locationProvider.html5Mode(true);

    $sceDelegateProvider.resourceUrlWhitelist(['**']);

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        title: 'Dark Sky Demo - Home'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        title: 'Dark Sky Demo - About'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
 .run(['$rootScope', '$location', function($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function (event, current) {
            $rootScope.title = current.$$route.title;
            $rootScope.isCollapsed = false;
            $rootScope.isActive = function (viewLocation) { 
                return viewLocation === $location.path();
            };
        });
    }]
 );
