'use strict';

/**
 * @ngdoc service
 * @name darkSkyDemoApp.geoLocalProvider
 * @description
 * # geoLocalProvider
 * Provider in the darkSkyDemoApp.
 */
angular.module('darkSkyDemoApp')
  .provider('geoLocalProvider', function () {

    // Private variables
    var ipinfoUrl = 'http://ipinfo.io';

    // Method for instantiating
    this.$get = ['$http', '$q', function ($http, $q) {

      /**
       * Get geo location based on IP
       * 
       * @returns {promise} - resolves with location data from the ipinfo service
       */
      function getGeoLocal() {
        var deferred = $q.defer();
        deferred.notify('About to request location info');

        return $http.jsonp(ipinfoUrl)
          .then(function (results) {
            return results.data;
          }, function (error) {
            $q.reject(error);
          })
          .catch(function (data, status) {
            $q.reject(status);
          });
      }

      var service = {
        getGeoLocal: getGeoLocal
      };

      return service;
    }];
  });
