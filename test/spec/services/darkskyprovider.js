'use strict';

describe('Service: darkSkyProvider', function () {

  // instantiate service
  var darkSkyProvider, $rootScope;

  beforeEach(module('darkSkyDemoApp'));
  beforeEach(inject(function(_$rootScope_, _darkSkyProvider_) {
    $rootScope = _$rootScope_;
    darkSkyProvider = _darkSkyProvider_;
  }));

  it('should do something', function () {
    expect(!!darkSkyProvider).toBe(true);
  });

  it('should have a function to get the forecast data', function(){
    expect(darkSkyProvider.getForecast).toBeDefined();
  });

  it('should get valid forecast data', function() {
    darkSkyProvider.getForecast().then(function(results){
      expect(results).not.toBeUndefined();
      expect(results.data).not.toBeUndefined();
    });
  });

});
