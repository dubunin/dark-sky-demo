'use strict';

/**
 * @ngdoc directive
 * @name darkSkyDemoApp.directive:footer
 * @description
 * # footer
 */
angular.module('darkSkyDemoApp')
  .directive('footer', function () {
        return {
            restrict: 'A', 
            replace: true,
            templateUrl: 'views/directives/footer.html',
            controller: ['$scope', '$filter', function ($scope) {
                $scope.date = new Date();
            }]
        };
  });
