'use strict';

/**
 * @ngdoc directive
 * @name darkSkyDemoApp.directive:header
 * @description
 * # header
 */
angular.module('darkSkyDemoApp')
  .directive('header', function () {
      return {
          restrict: 'A', 
          replace: true,
          templateUrl: 'views/directives/header.html',
          controller: ['$scope', '$filter', function () {
          }]
      };
  });
