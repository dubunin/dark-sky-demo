'use strict';

describe('Service: geoLocalProvider', function () {

  // instantiate service
  var geoLocalProvider, $rootScope, $httpBackend;

  // load the service's module
  beforeEach(module('darkSkyDemoApp'));
  beforeEach(inject(function(_$rootScope_, _geoLocalProvider_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    geoLocalProvider = _geoLocalProvider_;
    $httpBackend = _$httpBackend_;
  }));

  it('should do something', function () {
    expect(!!geoLocalProvider).toBe(true);
  });

  it('should have a function to get the geo local', function(){
    expect(geoLocalProvider.getGeoLocal).toBeDefined();
  });

  it('should get valid position data', function() {
    geoLocalProvider.getGeoLocal().then(function(position){
      expect(position).not.toBeUndefined();
    });
  });

  it('should handle invalid position data', function() {
    $httpBackend.expectGET('http://ipinfo.io').respond(404);

    geoLocalProvider.getGeoLocal().then(function(position){
      expect(position).toBeUndefined();
    });
  });

});
