'use strict';

/**
 * @ngdoc service
 * @name darkSkyDemoApp.darkSkyProvider
 * @description
 * # darkSkyProvider
 * Provider in the darkSkyDemoApp.
 */
angular.module('darkSkyDemoApp')
  .provider('darkSkyProvider', function () {

  // Private variables
  var config = {
    apiKey: '4b833c567ff70ca88e6d38db98af0665',
    baseUri: 'https://api.darksky.net/forecast/',
    units: 'us',
    language: 'en'
  };

  /**
   * Service provider definition
   */
  this.$get = ['$http', '$q', function($http, $q) {

    /** Public Methods */

    /**
     * Get provider configuration
     * @returns {object} - returns the current config object
     */
    function getConfig() {
      return config;
    }

    /**
     * Get daily weather forcast data
     * 
     * @param {number} latitude positition
     * @param {number} longitude positition
     * @returns {promise} - resolves with daily weather data object
     */
    function getForecast(latitude, longitude) {
      var url = config.baseUri + 
                config.apiKey + '/' + 
                latitude + ',' + 
                longitude + 
                '?units=' + config.units + 
                '&lang=' + config.language + 
                '&exclude=alerts,currently,flags,hourly,minutely';

      return $http.jsonp(url, {jsonpCallbackParam: 'callback'})
        .then(function(results){
          return results.data;
        })
        .catch(function(data, status) {
          return $q.reject(status);
        });
    }

    var service = {
      getForecast: getForecast,
      getConfig: getConfig
    };

    if (!config.apiKey) {
      console.warn('No Dark Sky API key set.');
    }
    return service;
  }];
});
